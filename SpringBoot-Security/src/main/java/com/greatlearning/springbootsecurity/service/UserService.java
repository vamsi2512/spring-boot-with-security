package com.greatlearning.springbootsecurity.service;

import com.greatlearning.springbootsecurity.entity.User;
import com.greatlearning.springbootsecurity.web.dto.UserRegistrationDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    User save(UserRegistrationDto registrationDto);
}
